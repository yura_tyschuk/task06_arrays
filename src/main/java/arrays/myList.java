package arrays;

public class myList {

  private int size;
  private String[] listOfStrings;

  myList(int size) {
    this.size = size;
    listOfStrings = new String[size];
  }

  private boolean checkIfArrIsFull() {
    boolean isFull = true;
    for (String s : listOfStrings) {
      if (s == null) {
        isFull = false;
        break;
      }
    }
    return isFull;
  }


  public void add(String elementToAdd) {
    if (listOfStrings[0] == null) {
      listOfStrings[0] = elementToAdd;
    } else if (checkIfArrIsFull()) {
      listOfStrings = reSize();
      listOfStrings[size - 1] = elementToAdd;
    } else {
      for (int i = 0; i < listOfStrings.length; i++) {
        if (listOfStrings[i] == null) {
          listOfStrings[i] = elementToAdd;
          break;
        }
      }
    }
  }

  public void print() {
    for (String i : listOfStrings) {
      System.out.println(i);
    }
  }

  private String[] reSize() {
    String[] tmp = new String[size];
    tmp = listOfStrings;
    size++;

    listOfStrings = new String[size];
    for (int i = 0; i < listOfStrings.length - 1; i++) {
      listOfStrings[i] = tmp[i];
    }
    return listOfStrings;
  }

  public String get(int index) {
    return listOfStrings[index];
  }

  public void set(String element, int index) {
    if (index < size) {
      listOfStrings[index] = element;
    }
  }

}
