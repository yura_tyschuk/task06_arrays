package arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogicalTask<T> {

  private List<Integer> thirdArr;

  public LogicalTask() {
    thirdArr = new ArrayList<Integer>();
  }

  public List<Integer> createThirdArrWithElementsInTwoArr(int[] firstArr, int[] secondArr) {
    for (int i = 0; i < firstArr.length; i++) {
      for (int j = 0; j < secondArr.length; j++) {
        if (firstArr[i] == secondArr[j]) {
          thirdArr.add(firstArr[i]);
        }
      }
    }

    return thirdArr;
  }

  public List<Integer> createThirdArrWithElementOnlyInOneArr(int[] firstArr, int[] secondArr) {

    for (int i = 0; i < firstArr.length; i++) {
      for (int j = 0; j < secondArr.length; j++) {
        if (firstArr[i] == secondArr[j]) {
          firstArr[i] = 0;
        }
      }
    }
    List<Integer> tmp = new ArrayList<Integer>();
    for (int i = 0; i < firstArr.length; i++) {
      if (firstArr[i] != 0) {
        tmp.add(firstArr[i]);
      }
    }
    System.out.println(tmp);
    return tmp;
  }

  public List<Integer> deleteAllElementsThatRepeatMoreThanTwoTimes(int[] array) {
    Map<Integer, Integer> mp = new HashMap<>();
    final int HOWMUCHREPEATS = 2;
    for (int i = 0; i < array.length; ++i) {
      mp.put(array[i], mp.get(array[i]) == null ? 1 : mp.get(array[i]) + 1);
    }

    for (int i = 0; i < array.length; ++i) {
      if (mp.containsKey(array[i]) && mp.get(array[i]) <= HOWMUCHREPEATS) {
        thirdArr.add(array[i]);
      }
    }
    return thirdArr;
  }


  public void deleteElementsInSequence(int[] array) {
    for (int i = 0; i <= array.length; i++) {
      if (array[i] == array[i + 1]) {
        thirdArr.add(array[i]);
      }
    }
    System.out.println(thirdArr);
  }

}

