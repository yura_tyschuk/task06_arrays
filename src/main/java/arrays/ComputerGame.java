package arrays;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ComputerGame {

  private static Logger logger = LogManager.getLogger();
  private int[] whatIsBehindTheDoor;
  private int[] whatCharacteristicsBehindTheDoor;
  private int heroStrength;
  private final int HEROSTRENGTH = 25;
  private int checkHowMuchDoorsWasOpened;
  private final Scanner scanner = new Scanner(System.in);
  private final Random random = new Random();
  private int choosenDoor;
  private List<Integer> sequenceOfDoors;

  public ComputerGame() {
    whatCharacteristicsBehindTheDoor = new int[10];
    whatIsBehindTheDoor = new int[10];
    sequenceOfDoors = new ArrayList<Integer>();
    heroStrength = HEROSTRENGTH;
    checkHowMuchDoorsWasOpened = 0;
  }


  private void createWhatIsBehindTheDoors() {
    for (int i = 0; i < whatIsBehindTheDoor.length; i++) {
      whatIsBehindTheDoor[i] = random.nextInt(2) + 1;
    }
  }

  public void createCharacteristicsBehindTheDoor() {
    createWhatIsBehindTheDoors();
    for (int i = 0; i < whatIsBehindTheDoor.length; i++) {
      if (whatIsBehindTheDoor[i] == 1) {
        whatCharacteristicsBehindTheDoor[i] = random.nextInt(100 - 5 + 1);
      } else if (whatIsBehindTheDoor[i] == 2) {
        whatCharacteristicsBehindTheDoor[i] = random.nextInt(80 - 10 + 1);
      }
    }

  }


  public void startGame() {

    createCharacteristicsBehindTheDoor();
    while (checkHowMuchDoorsWasOpened < 10) {
      System.out.println("Choose door[1-10]: ");
      choosenDoor = scanner.nextInt();
      if (openedDoor() == -1) {
        break;
      }

      whatIsBehindTheDoor[choosenDoor - 1] = 0;
      checkHowMuchDoorsWasOpened++;
      logger.trace("Doors was opened: " + checkHowMuchDoorsWasOpened);
    }

    if (checkHowMuchDoorsWasOpened == 10) {
      logger.fatal("You win!");
    }

  }

  private int openedDoor() {
    if (choosenDoor > 10) {
      return 1;
    } else {
      if (whatIsBehindTheDoor[choosenDoor - 1] == 1) {
        logger.warn(
            "Monster behind the door with characteristics: " + whatCharacteristicsBehindTheDoor[
                choosenDoor - 1]);
        if (fightWithMonster() == -1) {
          return -1;
        }
      } else if (whatIsBehindTheDoor[choosenDoor - 1] == 2) {
        logger.warn(
            "Artifact behind the door with characteristics: " + whatCharacteristicsBehindTheDoor[
                choosenDoor - 1]);
        pickUpArtefact();
      } else if (whatIsBehindTheDoor[choosenDoor - 1] == 0) {
        logger.warn("This door was opened! Choose another");
      }
    }
    return 0;
  }

  private int fightWithMonster() {
    if (heroStrength < whatCharacteristicsBehindTheDoor[choosenDoor - 1]) {
      heroStrength = -1;
      logger.fatal("You died!");
      return -1;
    } else if (heroStrength == whatCharacteristicsBehindTheDoor[choosenDoor - 1]) {
      heroStrength = 0;
      logger.fatal("You win!");
      logger.fatal("Your hp level =" + heroStrength);
    } else {
      heroStrength = heroStrength - whatCharacteristicsBehindTheDoor[choosenDoor - 1];
      logger.fatal("You win!");
      logger.fatal("Your hp level = " + heroStrength);
    }
    return heroStrength;
  }

  private int pickUpArtefact() {
    heroStrength = heroStrength + whatCharacteristicsBehindTheDoor[choosenDoor - 1];
    logger.fatal("Your strength become: " + heroStrength);
    return heroStrength;
  }

  public List<Integer> findInWhatSequenceHeroNeedToOpenTheDoors() {
    int sumOfArtefacts = HEROSTRENGTH;
    int sumOfMonsterDamage = 0;
    for (int i = 0; i < whatIsBehindTheDoor.length; i++) {
      if (whatIsBehindTheDoor[i] == 2) {
        sumOfArtefacts += whatCharacteristicsBehindTheDoor[i];
        sequenceOfDoors.add(i + 1);
      }
    }

    for (int i = 0; i < whatIsBehindTheDoor.length; i++) {
      if (whatIsBehindTheDoor[i] == 1) {
        sumOfMonsterDamage += whatCharacteristicsBehindTheDoor[i];
        sequenceOfDoors.add(i + 1);
      }
    }

    if (sumOfArtefacts - sumOfMonsterDamage < 0) {
      logger.fatal("HERO WILL DIE");
    } else {
      logger.trace("Hero will be alive!");
    }
    return sequenceOfDoors;
  }

  public void printDoors() {

    for (int i = 0; i < whatIsBehindTheDoor.length; i++) {
      if (whatIsBehindTheDoor[i] == 1) {
        logger.warn("Monster behind the door: " + whatCharacteristicsBehindTheDoor[i]);
        logger.warn("Door: " + (i + 1));
      } else if (whatIsBehindTheDoor[i] == 2) {
        logger.warn("Artifact behind the door: " + whatCharacteristicsBehindTheDoor[i]);
        logger.warn("Door: " + (i + 1));
      }
    }
  }


}
