package arrays;

public class TwoStringContainer implements Comparable<TwoStringContainer> {

  private String country;
  private String capital;

  TwoStringContainer(String country, String capital) {
    this.country = country;
    this.capital = capital;
  }

  @Override
  public int compareTo(TwoStringContainer o) {
    return country.compareTo(o.country);
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer("Two String Container{");
    sb.append("Country=").append(country);
    sb.append(", capital=").append(capital);
    sb.append('}');
    return sb.toString();
  }
}
