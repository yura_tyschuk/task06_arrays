package arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Deque {

  private static Logger logger = LogManager.getLogger();
  static final int MAX = 100;
  private int arr[];
  private int front;
  private int rear;
  private int size;

  public Deque(int size) {
    this.size = size;
    arr = new int[MAX];
    front = -1;
    rear = 0;
  }

  public boolean isFull() {
    return ((front == 0 && rear == size - 1) || front == rear + 1);
  }

  public boolean isEmpty() {
    return front == -1;
  }

  public void pushFront(int number) {
    if (isFull()) {
      logger.warn("Deque is full!");
      return;
    }
    if (front == -1) {
      front = 0;
      rear = 0;
    } else if (front == 0) {
      front = size - 1;
    } else {
      front--;
    }
    arr[front] = number;
  }

  public void pushBack(int number) {
    if (isFull()) {
      logger.warn("Deque is full!");
      return;
    }

    if (front == -1) {
      front = 0;
      rear = 0;
    } else if (rear == size - 1) {
      rear = 0;
    } else {
      rear++;
    }
    arr[rear] = number;
  }

  public void popFront() {
    if (isEmpty()) {
      logger.warn("Deque is empty!");
      return;
    }

    if (front == rear) {
      front = -1;
      rear = -1;
    } else {
      if (front == size - 1) {
        front = 0;
      } else {
        front++;
      }
    }
  }

  public void popBack() {
    if (isEmpty()) {
      logger.warn("Deque is empty!");
      return;
    }

    if (front == rear) {
      front = -1;
      rear = -1;
    } else if (rear == 0) {
      rear = size - 1;
    } else {
      rear--;
    }
  }

  public int getFront() {
    if (isEmpty()) {
      logger.warn("Deque is empty!");
      return -1;
    }
    return arr[front];
  }

  public int getRear() {
    if (isEmpty() || rear < 0) {
      logger.warn("Deque is empty!");
      return -1;
    }
    return arr[rear];
  }

}

