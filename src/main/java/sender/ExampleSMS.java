package sender;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
  public static final String ACCOUNT_SID = "AC6082273ecfca12d2fec7ae5a67f6530f";
  public static final String AUTH_TOKEN = "04719ebc99b7aa44a753f5d3a83583dd";
  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message
        .creator(new PhoneNumber("+380993400232"), /*my phone number*/
            new PhoneNumber("+12562697553"), str).create();
  }
}
