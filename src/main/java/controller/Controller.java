package controller;

import arrays.ComputerGame;
import arrays.LogicalTask;
import java.util.List;

public class Controller {

  ComputerGame computerGame;
  LogicalTask logicalTask;

  public Controller() {
    computerGame = new ComputerGame();
    logicalTask = new LogicalTask();
  }

  public void startGame() {
    computerGame.startGame();
  }

  public void printDoors() {
    computerGame.printDoors();
  }

  public List<Integer> findInWhatSequenceHeroNeedToOpenTheDoors() {
    return computerGame.findInWhatSequenceHeroNeedToOpenTheDoors();
  }

  public List<Integer> createThirdArrWithElementsInTwoArr(
      int[] firstArr, int[] secondArr) {
    return logicalTask.createThirdArrWithElementsInTwoArr(firstArr, secondArr);
  }

  public List<Integer> createThirdArrWithElementOnlyInOneArr(
      int[] firstArr, int[] secondArr) {
    return logicalTask.createThirdArrWithElementOnlyInOneArr(
        firstArr, secondArr);
  }

  public List<Integer> deleteAllElementsThatRepeatMoreThanTwoTimes(int[] array) {
    return logicalTask.deleteAllElementsThatRepeatMoreThanTwoTimes(
        array);
  }
}
