package generics;

import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Application {

  private static Logger logger = LogManager.getLogger();

  public static void main(String[] args) {
    GenericTest<Integer> tmp = new GenericTest<Integer>();
    tmp.setGeneric(15);
    logger.debug(tmp.unloadGeneric());
    GenericTest<String> tmp1 = new GenericTest<String>();
    tmp1.setGeneric("Test");
    logger.debug(tmp1.unloadGeneric());

    GenericTest2.genericDisplays(15.3);
    List<Integer> listOfIntegers = Arrays.asList(12, 13, 14);
    logger.debug("Sum of integers: " + GenericTest2.sumOfElem(listOfIntegers));
    List<Double> listOfDouble = Arrays.asList(12.1, 12.3, 12.5);
    logger.debug("Sum of double: " + GenericTest2.sumOfElem(listOfDouble));
    List<String> listOfString = Arrays.asList("Hello", "World", "!");
    GenericTest2.printElem(listOfDouble);
    GenericTest2.printElem(listOfString);
    PriorityQueue<String> pr = new PriorityQueue<String>(3);
    pr.insert("F", 6);
    pr.insert("S", 1);
    pr.insert("A", 25);
    pr.extractMax();
    System.out.println(pr.size());
    System.out.println(pr.isFull());
    pr.extractMax();
  }
}



