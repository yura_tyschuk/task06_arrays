package generics;

class GenericTest<T> {

  private T test;

  public void setGeneric(T test) {
    this.test = test;
  }

  public T unloadGeneric() {
    return this.test;
  }


}
