package generics;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GenericTest2 {

  private static Logger logger = LogManager.getLogger();

  static <T> void genericDisplays(T element) {
    System.out.println(element.getClass().getName() + "=" + element);
  }

  static <T> double sumOfElem(List<? extends Number> listToFindSum) {
    double sum = 0.0;
    for (Number i : listToFindSum) {
      sum += i.doubleValue();
    }

    return sum;
  }

  static <T> void printElem(List<? extends Object> list) {
    int tmp = 1;
    for (Object i : list) {
      logger.warn(tmp + ".Element = " + i);
      tmp++;
    }
  }
}
