package generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PriorityQueue<T extends Comparable<? super T>> implements Comparable {

  private int size;
  private List<T> list = new ArrayList<T>();
  private List<Integer> priorityList = new ArrayList<Integer>();
  private int capacity;

  public PriorityQueue(int size) {
    this.size = size;
    this.capacity = 0;
  }

  public PriorityQueue() {
    this.capacity = 0;
  }

  public void insert(T value, int priority) {
    if (value == null) {
      throw new NullPointerException();
    } else {
      list.add(value);
      priorityList.add(priority);
    }
  }

  private int minKey() {
    return Collections.min(priorityList);
  }

  private int findIndexOfMinKey() {
    for (int i = 0; i < priorityList.size(); i++) {
      if (minKey() == priorityList.get(i)) {
        return i;
      }
    }
    return 0;
  }

  public void extractMax() {
    int minKey = findIndexOfMinKey();
    System.out.println("Elem: " + list.get(minKey));
    System.out.println("Key: " + priorityList.get(minKey));
    list.remove(minKey);
    priorityList.remove(minKey);
  }

  public int size() {
    return list.size();
  }

  public boolean isFull() {
    return list.size() != size;
  }

  @Override
  public int compareTo(Object o) {
    return 0;
  }

}
