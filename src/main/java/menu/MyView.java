package menu;

import arrays.ComputerGame;
import arrays.LogicalTask;
import controller.Controller;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyView {

  private static Logger logger = LogManager.getLogger();
  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);
  ComputerGame computerGame;
  LogicalTask logicalTask;

  public MyView() {
    controller = new Controller();
    computerGame = new ComputerGame();
    logicalTask = new LogicalTask();
    menu = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    menu.put("1", "1 - Start a game");
    menu.put("2", "2 - Print who is behind the door");
    menu.put("3", "3 - Print in what sequnce hero need to open the doors");
    menu.put("4", "4 - Logical task 1 ");
    menu.put("5", "5 - Logical task 2");
    menu.put("6", "6 - Logical task 3");
    menu.put("Q", "Quit");

    methodsMenu.put("1", this::startGame);
    methodsMenu.put("2", this::printDoors);
    methodsMenu.put("3", this::printInWhatSequenceHeroNeedsToOpenTheDoors);
    methodsMenu.put("4", this::firstTask);
    methodsMenu.put("5", this::secondTask);
    methodsMenu.put("6", this::thirdTask);
  }

  private void outputMenu() {
    System.out.println("\n Menu: ");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void startGame() {
    controller.startGame();
  }

  private void printDoors() {
    controller.printDoors();
  }

  private void printInWhatSequenceHeroNeedsToOpenTheDoors() {
    logger.fatal(
        controller.findInWhatSequenceHeroNeedToOpenTheDoors());
  }

  private void firstTask() {
    int[] firstArr = new int[]{1, 2, 3, 4, 5};
    int[] secondArr = new int[]{3, 4, 5, 6, 7};

    logger.fatal(controller.createThirdArrWithElementOnlyInOneArr(
        firstArr, secondArr));
  }

  private void secondTask() {
    int[] firstArr = new int[]{1, 2, 3, 4, 5};
    int[] secondArr = new int[]{3, 4, 5, 6, 7};
    logger.fatal(controller.createThirdArrWithElementsInTwoArr(
        firstArr, secondArr));
  }

  private void thirdTask() {
    int[] firstArr = new int[]{3, 3, 3, 3, 3, 4, 5, 6, 7};
    logger.fatal(controller.deleteAllElementsThatRepeatMoreThanTwoTimes(firstArr));
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please select menu point: ");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();

      } catch (Exception e) {

      }
    } while (!keyMenu.equals("Q"));
  }


}
